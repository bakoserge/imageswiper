Swipecards
==========


A Tinder-like cards effect as of August 2014. You can swipe left or right to like or dislike the content.
The library creates a similar effect to Tinder's swipable cards with Fling animation.

It was inspired by [Kikoso's Swipeable-Cards] but I decided to create a more simple and fresh approach with the image loading to be more efficient.

It handles greatly asynchronous loading of adapter's data and uses the same layout parameters as FrameLayout (you may use `android:layout_gravity` in your layout xml file).

![ ](/screenshot.gif)

---


Installation
=======

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.lorentzos.swipecards/library/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.lorentzos.swipecards/library)

Go ahead find the latest version on [Gradle please] cowboy! 
Be sure to add the `@aar` suffix.


```groovy
dependencies {
    compile 'com.lorentzos.swipecards:library:X.X.X@aar'
}
```



Example
=======

The example is quite straightforward and documented in comments. 
You may find it under the relevant directory.


```java

//implement the onFlingListener
public class MyActivity extends FragmentActivity {

    private ArrayList<String> al;
    private ArrayAdapter<String> arrayAdapter;
    IconicAdapter ListAdapter;
    private int i;

    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private static final String IMAGE_CACHE_DIR = "thumbs_serge";

    @InjectView(R.id.frame) SwipeFlingAdapterView flingContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        ButterKnife.inject(this);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(getApplication(), IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getApplication(), mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(this.getSupportFragmentManager(), cacheParams);

        al = new ArrayList<>();
        al.add("https://cdn3.iconfinder.com/data/icons/simple-toolbar/512/iphone_apple_phone_mobile_zoom_touch_tap-128.png");
        al.add("https://cdn2.iconfinder.com/data/icons/modern-future-technology/128/tablet-128.png");
        al.add("https://cdn0.iconfinder.com/data/icons/intelligent-robots/800/Intelligent_Robots-27-128.png");
        al.add("https://cdn0.iconfinder.com/data/icons/intelligent-robots/800/Intelligent_Robots-1-128.png");
        al.add("https://dl.dropboxusercontent.com/u/5782460/android-dwn.jpg");
        al.add("https://cdn0.iconfinder.com/data/icons/intelligent-robots/800/Intelligent_Robots-5-128.png");
        al.add("https://dl.dropboxusercontent.com/u/5782460/android-dwn.jpg");
        al.add("https://cdn1.iconfinder.com/data/icons/iphone-ipad-android-gadgets/512/Gadget_Samsung-128.png");

        //arrayAdapter = new ArrayAdapter<>(this, R.layout.item, R.id.helloText, al );

        ListAdapter = new IconicAdapter(this,al);


        flingContainer.setAdapter(ListAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                al.remove(0);
                //arrayAdapter.notifyDataSetChanged();
                ListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                makeToast(MyActivity.this, "Left!");
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                makeToast(MyActivity.this, "Right!");
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                makeToast(MyActivity.this, "About to be empty, loading for data :D!");
                // Ask for more data here
                al.add("https://cdn3.iconfinder.com/data/icons/simple-toolbar/512/iphone_apple_phone_mobile_zoom_touch_tap-128.png");
                //arrayAdapter.notifyDataSetChanged();
                ListAdapter.notifyDataSetChanged();
                Log.d("LIST", "notified");
                i++;
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                makeToast(MyActivity.this, "Clicked!");
            }
        });

    }

    static void makeToast(Context ctx, String s){
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.right)
    public void right() {
        /**
         * Trigger the right event manually.
         */
        flingContainer.getTopCardListener().selectRight();
    }

    @OnClick(R.id.left)
    public void left() {
        flingContainer.getTopCardListener().selectLeft();
    }



    private class IconicAdapter extends BaseAdapter{

        Context context;
        ArrayList<String>images = new ArrayList<String>();
        public IconicAdapter(Context c,ArrayList<String>images){
            context = c;this.images=images;
        }

        @Override
        public int getCount() {
            return al.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(getApplication().LAYOUT_INFLATER_SERVICE);

            View imageView = inflater.inflate(R.layout.item, null);

            ImageView picture = (ImageView)imageView.findViewById(R.id.helloText);

            if (images.get(position)=="")
            {
                picture.setImageResource(R.drawable.ic_launcher);

            }
            else{
                //new DownloadImageTask(picture).execute(images.get(position));
                //picture.setImageResource(R.drawable.ic_launcher);
                //picture.setImageBitmap(getBitmapFromURL(images.get(position)));
                mImageFetcher.loadImage(images.get(position),picture);
            }



            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setLayoutParams(lp);

            return imageView;
        }
    }

    private ImageFetcher mImageFetcher;

    
}
```

You can alternatively use a helpful method which sets in one line both the listeners and the adapter.

```java
    // where "this" stands for the Context
    flingContainer.init(this, arrayAdapter);
````


**Adding buttons** is easy. Get the top card listener and trigger manually the right or left animation.
On the end of the animation the above listeners (e.g. removeFirstObjectInAdapter) will be triggered depending on the direction.
 

```java
    /**
     * Trigger the right event manually.
     */
    flingContainer.getTopCardListener().selectRight();
```



**Tip**: If you start a new Activity in the `onItemClicked` you will probably want to avoid double activity instances.
If so these solutions might work for you: [1](http://stackoverflow.com/a/8077776/1447885), 
[2](http://stackoverflow.com/a/17270364/1447885) and I personally prefer [3](http://stackoverflow.com/a/21906867/1447885)



Configuration
=============

You can optionally specify some attrs for the animation and the stack. The easiest way is in xml:

```xml
<com.lorentzos.flingswipe.SwipeFlingAdapterView
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    app:rotation_degrees="16"
    app:max_visible="4"
    app:min_adapter_stack="6" />
```

Or use styles:

```xml
<!-- Base application theme. -->
<style name="AppTheme" parent="android:Theme.Holo.Light.DarkActionBar">
    <!-- Customize your theme here. -->
    <item name="SwipeFlingStyle">@style/SwipeFling</item>
</style>
```

- rotation_degrees: the degrees of the card rotation offset
- max_visible: the max visible cards at the time
- min_adapter_stack: the min number of objects left. Initiates onAdapterAboutToEmpty() method.

License
======

```
   Copyright 2014 Dionysis Lorentzos

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

[Gradle please]:http://gradleplease.appspot.com/#com.lorentzos.swipecards
[Kikoso's Swipeable-Cards]:https://github.com/kikoso/Swipeable-Cards

[![Android Arsenal](https://img.shields.io/badge/Android%20Arsenal-Swipecards-brightgreen.svg?style=flat)](https://android-arsenal.com/details/1/1028)