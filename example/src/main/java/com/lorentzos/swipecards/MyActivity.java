package com.lorentzos.swipecards;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.lorentzos.swipecards.util.ImageCache;
import com.lorentzos.swipecards.util.ImageFetcher;
import com.lorentzos.swipecards.util.ImageWorker;

import javax.net.ssl.HttpsURLConnection;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;


public class MyActivity extends FragmentActivity {

    private ArrayList<String> al;
    private ArrayAdapter<String> arrayAdapter;
    IconicAdapter ListAdapter;
    private int i;

    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private static final String IMAGE_CACHE_DIR = "thumbs_serge";

    @InjectView(R.id.frame) SwipeFlingAdapterView flingContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        ButterKnife.inject(this);

        mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(getApplication(), IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getApplication(), mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(this.getSupportFragmentManager(), cacheParams);

        al = new ArrayList<>();
        al.add("https://cdn3.iconfinder.com/data/icons/simple-toolbar/512/iphone_apple_phone_mobile_zoom_touch_tap-128.png");
        al.add("https://cdn2.iconfinder.com/data/icons/modern-future-technology/128/tablet-128.png");
        al.add("https://cdn0.iconfinder.com/data/icons/intelligent-robots/800/Intelligent_Robots-27-128.png");
        al.add("https://cdn0.iconfinder.com/data/icons/intelligent-robots/800/Intelligent_Robots-1-128.png");
        al.add("https://dl.dropboxusercontent.com/u/5782460/android-dwn.jpg");
        al.add("https://cdn0.iconfinder.com/data/icons/intelligent-robots/800/Intelligent_Robots-5-128.png");
        al.add("https://dl.dropboxusercontent.com/u/5782460/android-dwn.jpg");
        al.add("https://cdn1.iconfinder.com/data/icons/iphone-ipad-android-gadgets/512/Gadget_Samsung-128.png");

        //arrayAdapter = new ArrayAdapter<>(this, R.layout.item, R.id.helloText, al );

        ListAdapter = new IconicAdapter(this,al);


        flingContainer.setAdapter(ListAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                al.remove(0);
                //arrayAdapter.notifyDataSetChanged();
                ListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                makeToast(MyActivity.this, "Left!");
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                makeToast(MyActivity.this, "Right!");
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                makeToast(MyActivity.this, "About to be empty, loading for data :D!");
                // Ask for more data here
                al.add("https://cdn3.iconfinder.com/data/icons/simple-toolbar/512/iphone_apple_phone_mobile_zoom_touch_tap-128.png");
                //arrayAdapter.notifyDataSetChanged();
                ListAdapter.notifyDataSetChanged();
                Log.d("LIST", "notified");
                i++;
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                makeToast(MyActivity.this, "Clicked!");
            }
        });

    }

    static void makeToast(Context ctx, String s){
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.right)
    public void right() {
        /**
         * Trigger the right event manually.
         */
        flingContainer.getTopCardListener().selectRight();
    }

    @OnClick(R.id.left)
    public void left() {
        flingContainer.getTopCardListener().selectLeft();
    }



    private class IconicAdapter extends BaseAdapter{

        Context context;
        ArrayList<String>images = new ArrayList<String>();
        public IconicAdapter(Context c,ArrayList<String>images){
            context = c;this.images=images;
        }

        @Override
        public int getCount() {
            return al.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(getApplication().LAYOUT_INFLATER_SERVICE);

            View imageView = inflater.inflate(R.layout.item, null);

            ImageView picture = (ImageView)imageView.findViewById(R.id.helloText);

            if (images.get(position)=="")
            {
                picture.setImageResource(R.drawable.ic_launcher);

            }
            else{
                //new DownloadImageTask(picture).execute(images.get(position));
                //picture.setImageResource(R.drawable.ic_launcher);
                //picture.setImageBitmap(getBitmapFromURL(images.get(position)));
                mImageFetcher.loadImage(images.get(position),picture);
            }



            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setLayoutParams(lp);

            return imageView;
        }
    }

    private ImageFetcher mImageFetcher;

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

                //makeToast(getApplicationContext().getApplicationContext(),"loading this next image");
                /*URL newurl = new URL(urldisplay);
                mIcon11 = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());*/


            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            //
            bmImage.setImageBitmap(decodeSampledBitmapFromResource(getResources(),
                    R.id.helloText,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
            bmImage.setImageBitmap(result);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,int reqWidth, int reqHeight) {
        Bitmap mIcon11 = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * Direct method to download the images
     * @param src
     * @return
     */
    static Bitmap myBitmap;
    public static Bitmap getBitmapFromURL(final String src) {

        try {
            Thread t = (new Thread(){

                @Override
                public void run() {
                    try
                    {


                        Log.e("src", src);

                        URL newurl = new URL(src);
                        myBitmap = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());

                        /*URL url = new URL(src);
                        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        myBitmap = BitmapFactory.decodeStream(input);*/
                        Log.e("Bitmap","returned");
                        //return myBitmap;
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Log.e("Exception",e.getMessage());
                    }

                }
            });

            t.start();
        }
        catch(Exception ex){
            Log.e("Exception",ex.getMessage());
            return null;
        }
        return myBitmap;
    }

}
