package com.lorentzos.swipecards.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.ShutterCallback;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.lorentzos.swipecards.R;
import com.lorentzos.swipecards.logger.Log;
import com.lorentzos.swipecards.util.Utils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by serge on 08/07/2015.
 */
public class CameraActivity extends Activity {
    private static final String TAG = "CameraDemo";
    SquaredSurfaceView preview; // <1>
    FrameLayout buttonClick; // <2>
    int rotation=0;
    OrientationEventListener orientationEventListener;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_preview);

        Toast.makeText(getBaseContext(), "Touch the screen to take picture.", Toast.LENGTH_SHORT).show();

        final Camera camera = Camera.open();
        preview = new SquaredSurfaceView(this,camera); // <3>
        ((FrameLayout) findViewById(R.id.preview)).addView(preview); // <4>

        //buttonClick = (Button) findViewById(R.id.buttonClick);

        buttonClick = (FrameLayout) findViewById(R.id.preview);

        buttonClick.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { // <5>
                camera.takePicture(shutterCallback, rawCallback, jpegCallback);
                /*preview.camera.takePicture(shutterCallback, rawCallback, jpegCallback);*/
            }
        });

        if (orientationEventListener == null)
        {


            orientationEventListener = new OrientationEventListener(CameraActivity.this, SensorManager.SENSOR_DELAY_NORMAL) {
                @Override
                public void onOrientationChanged(int angle) {

                    Camera.Parameters parameters = camera.getParameters();

                    switch (angle) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotation = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotation = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotation = 90;
                            break;
                    }
                    parameters.setRotation(rotation);
                    camera.setParameters(parameters);
                    //...Do work using getActivity()...
                    //Toast.makeText(CameraActivity.this,"Orientation changed = " + angle,Toast.LENGTH_SHORT).show();
                }
            };
        }

        Log.d(TAG, "onCreate'd");
    }

    @Override
    public void onResume() {
        super.onResume();
        orientationEventListener.enable();
    }

    @Override
    public void onPause() {
        super.onPause();
        orientationEventListener.disable();
    }

    // Called when shutter is opened
    ShutterCallback shutterCallback = new ShutterCallback() { // <6>
        public void onShutter() {
            Log.d(TAG, "onShutter'd");
        }
    };

    //Handles data for raw picture
    Camera.PictureCallback rawCallback = new Camera.PictureCallback() { // <7>
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.d(TAG, "onPictureTaken - raw");


        }
    };


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /***
     *
     * @param sizes
     * @param w
     * @param h
     * @return
     */
    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                com.lorentzos.swipecards.provider.Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                android.util.Log.d(TAG, "Oops! Failed create "
                        + com.lorentzos.swipecards.provider.Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Uri fileUri;

    // Handles data for jpeg picture
    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() { // <8>
        public void onPictureTaken(byte[] data, Camera camera) {


            FileOutputStream outStream = null;

            try {

                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                //Write to SD Card
                outStream = new FileOutputStream(
                        String.format(
                                fileUri.getPath(),//Environment.getExternalStorageDirectory() +
                                System.currentTimeMillis()
                        )); // <9>

                outStream.write(data);
                outStream.close();

                Toast.makeText(getBaseContext(), "Preview", Toast.LENGTH_SHORT).show();


                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length);

                //call the preview activity with this bundle
                Intent i = new Intent(CameraActivity.this, UploadActivity.class);
                i.putExtra("filePath", Utils.getPath(CameraActivity.this, fileUri));//
                i.putExtra("isImage", true);

                startActivity(i);

            } /*catch (FileNotFoundException e) { // <10>
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            catch (Exception e){
                e.printStackTrace();
            }
            finally {

            }
            Log.d(TAG, "onPictureTaken - jpeg");
        }
    };

    /**
     * Get the real path of Uri, when we select an image from gallery and we get the path with the code
     * ie :  /content:/media/external/images/media/34343
     * @param context
     * @param contentUri
     * @return
     */
    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
