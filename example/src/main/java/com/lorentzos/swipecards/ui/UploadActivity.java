package com.lorentzos.swipecards.ui;

/**
 * Created by serge on 01/04/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.lorentzos.swipecards.R;
import com.lorentzos.swipecards.provider.Config;
import com.lorentzos.swipecards.util.AndroidMultiPartEntity;
import com.lorentzos.swipecards.util.Utils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class UploadActivity extends Activity {
    // LogCat tag
    private static final String TAG = ImageUploaderActivity.class.getSimpleName();

    private ProgressBar progressBar;
    private String filePath = null;
    private TextView txtPercentage;
    private ImageView imgPreview;
    private VideoView vidPreview;
    private Button btnUpload;
    long totalSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        vidPreview = (VideoView) findViewById(R.id.videoPreview);

        // Changing action bar background color
        getActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor(getResources().getString(
                        R.color.action_bar))));

        // Receiving the data from previous activity
        Intent i = getIntent();

        // image or video path that is captured in previous activity
        filePath = i.getStringExtra("filePath");

        // boolean flag to identify the media type, image or video
        boolean isImage = i.getBooleanExtra("isImage", true);

        if (filePath != null) {
            // Displaying the image or video on the screen
            previewMedia(isImage);
        }else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // uploading the file to server
                new UploadFileToServer().execute();
            }
        });

    }

    /**
     * Displaying captured image/video on the screen
     */
    private void previewMedia(boolean isImage) {
        // Checking whether captured media is image or video
        if (isImage) {
            imgPreview.setVisibility(View.VISIBLE);
            vidPreview.setVisibility(View.GONE);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            /*options.inSampleSize = 8;*/
            //options.inPreferQualityOverSpeed = true;
            Uri uri = Uri.fromFile(new File(filePath));

            int rotate = 0;
            try {

                final Bitmap bitmap = BitmapFactory.decodeFile(filePath,options);

                /*DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);

                int width = imgPreview.getMeasuredWidth();
                int height = imgPreview.getMeasuredHeight();

                float imageOriginalWidthHeightRatio = (float) bitmap.getWidth() / (float) bitmap.getHeight();

                //Calculate the new width and height of the image to display
                int imageToShowHeight = metrics.heightPixels - height;
                int imageToShowWidth = (int) (imageOriginalWidthHeightRatio * imageToShowHeight);


                //Adjust the image width and height if bigger than screen
                if(imageToShowWidth > metrics.widthPixels)
                {
                    imageToShowWidth = metrics.widthPixels;
                    imageToShowHeight = (int) (imageToShowWidth / imageOriginalWidthHeightRatio);
                }

                //Create the new image to be shown using the new dimensions
                Bitmap imageToShow = Bitmap.createScaledBitmap(bitmap, imageToShowWidth, imageToShowHeight, true);

*/
                //imgPreview.setMinimumWidth(width);
                int orientation = Utils.getOrientation(this, uri, filePath);
                imgPreview.setImageBitmap(bitmap);


            } catch (Exception e) {
                e.printStackTrace();
            }




            //Toast.makeText(this, ConvertImageToBase64(filePath), Toast.LENGTH_LONG).show();

        }
        else {
            imgPreview.setVisibility(View.GONE);
            vidPreview.setVisibility(View.VISIBLE);
            vidPreview.setVideoPath(filePath);
            // start playing
            vidPreview.start();
        }
    }

    /**
     * Converts an images to its Base64 equivalent
     * NB: This make the image file 33% larger
     * @param filePath
     * @return
     */
    public String ConvertImageToBase64(String filePath){
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            options.inSampleSize = 4;
            Bitmap bm = BitmapFactory.decodeFile(filePath,options);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            bm.compress(Bitmap.CompressFormat.JPEG,40,baos);

            // bitmap object

            byte [] byteImage_photo = baos.toByteArray();

            //generate base64 string of image

            String encodedImage = Base64.encodeToString(byteImage_photo, Base64.DEFAULT);
            return encodedImage;
        }
        catch (Exception e){
            return "Failed";
        }
    }

    /**
     * Uploading the file to server
     */
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.FILE_UPLOAD_URL);

            String boundary = "-------------" + System.currentTimeMillis();

            httppost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(filePath);



                // Extra parameters if you want to pass to server
                try {
                    FileBody fileBody = new FileBody(sourceFile);

                    // Adding file data to http body
                    entity.addPart("image", fileBody);
                    entity.addPart("website", new StringBody("www.androidhive.info"));

                    entity.addPart("email", new StringBody("abc@gmail.com"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                catch (Exception e){
                    int i = 0;
                }


                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = null;
                try {
                    response = httpclient.execute(httppost);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            // showing the server response in an alert dialog
            showAlert(result);

            super.onPostExecute(result);
        }

        /**
         * Method to show alert dialog
         */
        private void showAlert(String message) {
            AlertDialog.Builder builder = new AlertDialog.Builder(UploadActivity.this);
            builder.setMessage(message).setTitle("Response from Servers")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // do nothing
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }
}
