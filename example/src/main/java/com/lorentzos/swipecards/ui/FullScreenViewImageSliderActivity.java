package com.lorentzos.swipecards.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import com.lorentzos.swipecards.R;
import com.lorentzos.swipecards.util.ImageCache;
import com.lorentzos.swipecards.util.ImageFetcher;
import com.lorentzos.swipecards.util.Utils;

import java.util.ArrayList;

public class FullScreenViewImageSliderActivity extends FragmentActivity {

    private ViewPager viewPager;
    private Utils utils;
    private ArrayList<String> imagesPath = new ArrayList<>();

    private FullScreenImageAdapter fullScreenImageAdapter;

    private ImageFetcher mImageFetcher;
    private static final String IMAGE_CACHE_DIR = "images";
    public static final String EXTRA_IMAGE = "extra_image";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(this.getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(false);

        viewPager = (ViewPager)findViewById(R.id.pager);
        utils = new Utils(this);

        imagesPath = utils.getFilePaths();

        fullScreenImageAdapter = new FullScreenImageAdapter(FullScreenViewImageSliderActivity.this,imagesPath);

        viewPager.setAdapter(fullScreenImageAdapter);
    }
    public ImageFetcher getImageFetcher() {
        return mImageFetcher;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_full_screen_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
