package com.lorentzos.swipecards.provider;

import java.util.Arrays;
import java.util.List;

/**
 * Created by serge on 01/04/2015.
 */
public class Config {
    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = "http://87.198.178.66:56399/server/ImageHandler.ashx";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Android_File_Upload";

    // SD card image directory
    public static final String PHOTO_ALBUM = "DCIM/Camera";

    // supported file formats
    public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg",
            "png");
}
