package com.lorentzos.swipecards.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ImageView;

/**
 * Created by serge on 10/04/2015.
 * To allow imageView to be full screen. while keeping ratio
 */
public class AspectRatioImageView extends ImageView {
    private Context _context;
    public AspectRatioImageView(Context context) {
        super(context);
        _context = context;
    }
    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _context = context;
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        _context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
        setMeasuredDimension(width, height);

    }
}
